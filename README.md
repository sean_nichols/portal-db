# portal-db

### About

_Simple Ingress portal database manager_:

**portal-db** is a simple Python application to keep track of Ingress portals within a given geographic area. Features include:

 * Importing from the Intel map (in a limited fashion so as to stay compliant with Niantic TOS) and from agent personal data dumps;
 * Exporting to KML and/or JSON;
 * Editing portal fields (e.g.: name, image);
 * Tracking personal portal attributes (e.g.: visits and captures);
 * Basic list management (adding, removing, filtering, etc.);
 * (Optional) S2 cell geocoding and location; 
 * (Optional) PHP front end for deploying to web (mostly limited to importing personal data dumps and tracking visits/attributes).

### Requirements and Dependencies

**portal-db** requires Python 3.4.

The optional web front end requires PHP 5.4.

The optional S2 cell/grid features require the installation of the [s2sphere Python library](http://s2sphere.sidewalklabs.com/en/latest/).

### Installation

##### For portal-db:

 * Ensure you have Python 3.4+ installed.
 * Download `portal_db.py` and put it in a fresh directory.
 * If you want to use the S2 cell capabilities, install the [s2sphere Python library](http://s2sphere.sidewalklabs.com/en/latest/) (instructions on that site; pip is generally the easiest way); otherwise you can skip this step (and/or return to it later).
 * From the command line, enter the directory you have created, and run `python3 portal_db.py`. If you get help/usage text, congratulations!

(This assumes that the `python3` executable is in your path. Otherwise, you may have to fully qualify it, or it may just be called `python` instead of `python3`, etc.)

##### For the web front-end:

 * Ensure you have a web server with PHP 5.4+ installed and accessible from a web browser (public or not, as is your preference).
 * Download the contents of the [web-front-end](web-front-end) directory in this repository (specifically: `index.php`, `server_conf.php`, `portal_map.css` and `portal_map.js`, as well as the `images/` subdirectory).
 * Create a fresh directory on the server (hereafter referred to as _portals_/, but rename it as you desire).
 * Edit `server_conf.php` to ensure the directory, host name, etc. match your server. (You will need to add to the `$KNOWN_LOCATIONS` array whenever you add a JSON portal table (see later); the default setup includes an entry for the *San Francisco Ferry Building*, as found in the examples, and detailed in the Quick-Start section below.)
 * Copy the downloaded PHP, CSS and JS files into your newly created `portals/` directory. Also copy the `images/` directory in (so it is called `portals/images/`).
 * Download the latest _OpenLayers_ distribution (the `*-dist.zip` file) from [https://openlayers.org/download/](https://openlayers.org/download/). Extract the files `ol.css` and `ol.jss` from the zip file and put them in the `portals/` directory.
 * Create a blank subdirectory on called `basemaps/` (so it is called `portals/basemaps/`). This is where you will add JSON portal tables (exported from **portal-db**) to be able to generate maps of unvisited portals (otherwise, you will only be able to generate maps of already-visited portals).   
 * Finally, create another blank subdirectory called `profiles/` (so it is called `portals/profiles/`).

### Quick Start

_To be completed._

For now:

 * Generate a text file containing a list of portals of the form:  
   `<portal intel URL> [!<portal image URL>] [<portal name>]`  

    * An example is the `sf_ferry_building.txt` file in this repository under `examples/`. Excerpt:  
      ```https://intel.ingress.com/intel?ll=37.795556,-122.393434&z=17&pll=37.795556,-122.393434 !https://lh4.ggpht.com/6gXiFumzMcUQHstlDTDX2xZndzCRRId8Wi-d3Y3flrFiaJdgBD1JhSoANoIFKaKxN18hCjrUCarm03rX6dbn Port of San Francisco
      https://intel.ingress.com/intel?ll=37.795317,-122.393252&z=17&pll=37.795317,-122.393252 !https://lh5.ggpht.com/_u8adQwOLNL5SozfpL9pQZZauDWS-4aXQ0wmzpPTqaLayoFwLFBGYg7GqBoMkZpwN_NbaKd1ZJOB0S_LyCI Olives Mosaic
      https://intel.ingress.com/intel?ll=37.795232,-122.393502&z=17&pll=37.795232,-122.393502 !https://lh5.ggpht.com/C50KTpLaGu2M82VLbAzbg3ZXj_ktFIOm3vn8-Wk-gGD_5NKKGD97daSRs-5_cEvWMa1EYnu8lTpeLjqepwk The Great Seal of California
      https://intel.ingress.com/intel?ll=37.79537,-122.393812&z=17&pll=37.79537,-122.393812 !https://lh4.ggpht.com/5qZFdObTvlM6JxVotfohA64ZE_VrPpfcXEa-yHWTYDtWr4Ob_NQQxwdhf-mEkQpyZtB6Nth30zGngCPeVD4 Ferry Building Clock Tower```

 * Create a new table in the database by importing the text file:  
   `python3 portal_db.py add sf_ferry_building.txt`

 * Export the table to KML:  
   `python3 portal_db.py print --kml > 20190205_Ferry_Building_portals.kml`

 * Export the table to JSON (for use by the web front end):
   `python3 portal_db.py print --json --fields "latlong,name,image" > 20190205_Ferry_Building_portals.json`  
   (You can now place this file in the `basemaps/` folder on the web server.)

 * There is more help available at the command line. Access it by adding `--help` to the command. E.g.:  
   `python3 portal_db.py --help`  
   `python3 portal_db.py add --help`  
   `python3 portal_db.py print --help`  
   _etc..._

### License

Fooey to licenses.

**portal-db** is public domain: feel free to abuse this code as you see fit!

(But I wouldn't mind you dropping me a line to let me know if you happen to do something really cool with it...)

### Contact

(But it's still copyright. Not that I'm going to make a big deal of it.)
 
Copyright 2018-2019 Sean C. Nichols

 * email `seanni@trichotomy.ca`
 * ingress `@sirputter`
 * web `http://trichotomy.ca/`
 * git `https://bitbucket.org/sean_nichols/portal-db`

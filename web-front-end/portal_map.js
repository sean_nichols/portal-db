var ol_kml_layer = null;
var ol_map_obj = null;

var mapsets = {
    city : [ 'unvisited', 'visited', 'captured' ],
    world : [ 'visited', 'captured' ]
};

function init() {
    initProfilePage();
    initIndexPage();
}

function _element( domid ) {
    return document.getElementById( domid );
}

function initProfilePage() {
    var mapselects = document.getElementsByClassName( 'mapsetselect' );
    if (! mapselects.length) {
        // Not on the profile page
        return;
    }

    for (var i = 0; i < mapselects.length; i++) {
        mapselects[ i ].checked = false;
    }
    
    for (var map in mapsets) {
        for (var i = 0; i < mapsets[ map ].length; i++) {
            var selid = map + '_' + mapsets[ map ][ i ] + '_clr';
            _element( selid ).selectedIndex = 0;
        }
        _element( map + '_submit' ).disabled = true;
    }
    
    initOLMap();
}

function initIndexPage() {
}

function initOLMap() {
    if (! document.getElementsByClassName( 'ol_map' ).length) {
        // No OL objects
        return;
    }

    ol_map_obj = new ol.Map( {
        target : 'ol_map',
        layers : [
            new ol.layer.Tile( {
                source : new ol.source.OSM()
            } )
        ],
        view   : new ol.View( {
            // projection : robinson,
            center     : ol.proj.fromLonLat( [0, 45] ),
            zoom       : 1
        } )
    } );
}

function addOLMapLayer( kml_url ) {
    if (ol_kml_layer !== null) {
        ol_map_obj.removeLayer( ol_kml_layer );
    }
    
    ol_kml_layer = new ol.layer.Vector( {
        source : new ol.source.Vector( {
            url    : kml_url,
            format : new ol.format.KML()
        } )
    } );
    
    ol_map_obj.addLayer( ol_kml_layer );
}

function validateMapSelection( mapid ) {
    var mapset = mapsets[ mapid ];
    var cansubmit = false;

    for (var i = 0; i < mapset.length; i++) {
        var setid = mapid + '_' + mapset[ i ];
        var checked = _element( setid ).checked;
        var pindiv = _element( setid + '_pin' );

        cansubmit = cansubmit || checked;
        pindiv.style.display = (checked ? 'inline-block' : 'none');
    }
    
    _element( mapid + '_submit' ).disabled = !cansubmit;
    _element( mapid + '_preview' ).disabled = !cansubmit;
}

function updateImage( setid ) {
    var clrsel = _element( setid + '_clr' );
    var pinimg = _element( setid + '_img' );
    var clrid = clrsel[ clrsel.selectedIndex ].value;
    
    pinimg.src = PIN_IMAGE_FOLDER + clrid + '_pin.png';
}

function getMapdef( mapid ) {
    var mapset = mapsets[ mapid ];
    var mapdef = '';

    if (mapid == 'world') {
        mapdef += '00';
    }
    
    for (var i = 0; i < mapset.length; i++) {
        var setid = mapid + '_' + mapset[ i ];
        if (_element( setid ).checked) {
            var clrsel = _element( setid + '_clr' );
            mapdef += '1' + clrsel.selectedIndex;
        } else {
            mapdef += '00';
        }
    }
    
    return mapdef;
}

function getMapbase( mapid ) {
    var basesel = _element( mapid + '_base' );
    return basesel[ basesel.selectedIndex ].value;
}

function submitMapRequest( mapid ) {
    _element( mapid + '_map' ).value = getMapdef( mapid );
    
    if (mapid != 'world') {
        _element( mapid + '_baseid' ).value = getMapbase( mapid );
    }
    
    _element( mapid + '_mapreq' ).submit();
}

function previewMapRequest( mapid ) {
    var kml_url = window.location;
    
    var mapdef = getMapdef( mapid );
    if (mapid == 'world') {
        kml_url += '&world=' + mapdef;
    } else {
        kml_url += '&map=' + mapdef + '&base=' + getMapbase( mapid );
    }
    kml_url += '&embed=y';

    addOLMapLayer( kml_url );
}

function showFAQ( q ) {
    var a = _element( q.id.slice( 0, -1 ) + 'a' );
    
    q.classList.toggle( 'hidden' );
    q.classList.toggle( 'shown' );
    a.classList.toggle( 'hidden' );
    a.classList.toggle( 'shown' );
}

function showImgFullsize( img ) {
    img.classList.toggle( 'thumb' );
}

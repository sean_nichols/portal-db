<?php

define( 'PID_LENGTH',   6 );
define( 'TSV_TIME',     0 );
define( 'TSV_LAT',      1 );
define( 'TSV_LNG',      2 );
define( 'TSV_TRIGGER',  3 );
define( 'TSV_COMMENTS', 4 );

define( 'NL', "\n" );
define( 'DATE_FORMAT', 'Y-m-d @ H:i:s T' );

define( 'ERR_COULD_NOT_OPEN', 512 );
define( 'ERR_NOT_LOG_FILE',   513 );

$pin_colors = array(
    'red' => 'red',
    'ylw' => 'yellow',
    'blu' => 'blue',
    'grn' => 'green',
    'prp' => 'purple',
    'rng' => 'orange',
    'brn' => 'brown',
    'pnk' => 'pink'
);

require_once( 'server_conf.php' );

function echo_r( $var ) {
    echo '<pre>' . print_r( $var, true ) . '</pre>' . NL;
}

function showUploadError( $errcode ){
    $msg = '';

    switch ($errcode) {
        case UPLOAD_ERR_OK:
            $msg = NULL;
            break;
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            $msg = 'Uploaded file too large. Max filesize: ' . MAX_UPLOAD_SIZE . ' bytes.';
            break;
        case UPLOAD_ERR_PARTIAL:
            $msg = 'Upload interrupted before entire file received.';
            break;
        case UPLOAD_ERR_NO_FILE:
            $msg = 'No file received.';
            break;
        case UPLOAD_ERR_NO_TMP_DIR:
        case UPLOAD_ERR_CANT_WRITE:
            $msg = 'Unable to write uploaded file to disk.';
            brea;
        case ERR_COULD_NOT_OPEN:
            $msg = 'Unable to read uploaded file.';
            return $msg;
        case ERR_NOT_LOG_FILE:
            $msg = 'Uploaded file does not seem to be an Ingress log file. ' .
                   'Verify that you are trying to upload \'game_log.tsv\'.';
            break;
        default:
            $msg = 'Unknown error with upload.';
            break;
    }

    return $msg;
}

function profileUploaded( $formdata ) {
    $upload = handleUpload( $formdata );

    if (! $upload[ 'created' ]) {
        $error = showUploadError( $upload[ 'errcode' ] );
        return mkPage_( mkIndexPage(), $error );
    }
    
    return mkPage_( mkProfilePage( $upload[ 'pid' ] ) );
}

function handleUpload( $formdata ) {
    $agent = $formdata[ 'agent' ];
    $log   = $_FILES[ 'game_log' ];
    $ts    = time();

    $filename = $log[ 'tmp_name' ];
    $errcode  = $log[ 'error' ];

    if ($errcode) {
        return array( 'created' => false, 'errcode' => $errcode );
    }

    $parseret = parseLog( $filename );
    if (! $parseret[ 'created' ]) {
        return $parseret;
    }

    $ptree = $parseret[ 'ptree' ];
    $file_json = json_encode(
        array(
            'agent'     => $agent,
            'timestamp' => $ts,
            'log'       => $ptree
        )
    );

    $pid = $ptree[ 'pid' ];
    file_put_contents( profileFilename( $pid ), $file_json );

    return array( 'created' => true, 'pid' => $pid );
}

function base64url_encode( $data ) {
    return rtrim( strtr( base64_encode( $data ), '+/', '-_'), '=' );
}

function array_amap( $fn, $arr ) {
    // php is FUCKING stupid

    $r = array();

    foreach ($arr as $k => $v) {
        $r[] = $fn( $k, $v );
    }

    return $r;
}

function profileFilename( $pid ) {
    return PROFILES_FOLDER . $pid . '.json';
}

function generatePID( $logfile ) {
    while (true) {
        $hash = hash( 'sha256', $logfile . time(), true );
        $pid = substr( base64url_encode( $hash ), 0, PID_LENGTH );
        if (! profileExists( $pid )) {
            return $pid;
        }
    }
}

function parseLog( $logfile ) {
    $events = array();
    $generated = NULL;

    $TSV_HEADER = implode( "\t", array(
        'Event Time',
        'Event Lat',
        'Event Lng',
        'Tracker Trigger',
        'Comments'
    ) );
    $VISIT_ACTIONS = array(
        'created link',
        'hacked enemy portal',
        'hacked friendly portal',
        'hacked neutral portal',
        'resonator deployed',
        'resonator upgraded',
        'mod deployed'
    );


    $fh = fopen( $logfile, 'r' );
    if ($fh) {
        $header = trim( fgets( $fh ) );
        if ($header == $TSV_HEADER ) {
            // We're /probably/ ok...
            while (($line = fgets( $fh )) !== false) {
                $event = explode( "\t", trim( $line ) );
                if (! $generated) {
                    $dt = DateTime::createFromFormat( 'Y m-d H:i:s', $event[ TSV_TIME ] );
                    $generated = $dt->getTimestamp();
                }
                $coord = $event[ TSV_LAT ] . ',' . $event[ TSV_LNG ];
                if ($event[ TSV_TRIGGER ] == 'captured portal') {
                    $events[ $coord ] = 'c';
                } elseif (in_array( $event[ TSV_TRIGGER ], $VISIT_ACTIONS )) {
                    if (! array_key_exists( $coord, $events )) {
                        $events[ $coord ] = 'v';
                    }
                }
            }
        } else {
            return array( 'created' => false, 'errcode' => ERR_NOT_LOG_FILE );
        }
        fclose( $fh );
    } else {
        return array( 'created' => false, 'errcode' => ERR_COULD_NOT_OPEN );
    }

    if ($events) {
        $visited  = array();
        $captured = array();

        foreach ($events as $coord => $vc) {
            if ($vc == 'v') {
                $visited[] = $coord;
            } elseif ($vc == 'c') {
                $captured[] = $coord;
            }
        }
        $portals = compact( 'visited', 'captured' );
    } else {
        $portals = NULL;
    }

    $pid = generatePID( $logfile );
    return array(
        'created' => true,
        'ptree'   => compact( 'pid', 'portals', 'generated' )
    );
}

function profileExists( $pid ) {
    return file_exists( profileFilename( $pid ) );
}

function hasProfile() {
    foreach (array_merge( $_POST, $_GET ) as $k => $v) {
        if (strlen( $k ) == PID_LENGTH && ! $v) {
            if (profileExists( $k )) {
                return $k;
            }
        }
    }
    return false;
}

function readProfile( $pid ) {
    return json_decode( file_get_contents( profileFilename( $pid ) ), true );
}

function getBasemapPortalList( $baselist ) {
    $portals = array();
    foreach ($baselist as $pdata) {
        list( $lat, $long ) = $pdata[ 'latlong' ][ 1 ];
        $name = $pdata[ 'name' ][ 1 ];
        $portal = array(
            'lat' => $lat,
            'long' => $long,
            'name' => $name
        );
        if (array_key_exists( 'added', $pdata )) {
            $portal[ 'added' ] = $pdata[ 'added' ][ 1 ];
        }
        if (array_key_exists( 'image', $pdata )) {
            $portal[ 'image' ] = $pdata[ 'image' ][ 1 ];
        }

        $portals[] = $portal;
    }
    
    return $portals;
}

function readBasemap( $filename ) {
    return json_decode( file_get_contents( BASEMAPS_FOLDER . $filename . '.json' ), true );
}

function compMapName( $a, $b ) {
    if ($a[ 'name' ] == $b[ 'name' ]) {
        return strcmp( $b[ 'date' ], $a[ 'date' ]);
    } else {
        return strcasecmp( $a[ 'name' ], $b[ 'name' ] );
    }
}

function listBasemaps( $locales ) {
    $locales = array_map(
        function( $l ) {
            return str_replace( ' ', '_', $l );
        },
        $locales
    );
    
    $maps = array();
    $mapfiles = scandir( substr( BASEMAPS_FOLDER, 0, -1 ) );
    rsort( $mapfiles );
    foreach ($mapfiles as $filename) {
        foreach ($locales as $locale) {
            if (stripos( $filename, $locale ) !== false) {
                $basename = substr( $filename, 0, strpos( $filename, '.' ) );
                $words = explode( '_', $basename );
                $datestr = array_shift( $words );
                $date = substr( $datestr, 0, 4 ) . '-' . substr( $datestr, 4, 2 ) . '-' . substr( $datestr, 6 );
                $name = implode( ' ', $words );
                $maps[] = array(
                    'name'  => $name,
                    'date'  => $date,
                    'label' => $name . ' ' . $date,
                    'file'  => $basename
                );
                break;
            }
        }
    }
    
    usort( $maps, 'compMapName' );

    return $maps;
}

function mostRecentBasemaps( $locales ) {
    $all_basemaps = listBasemaps( $locales );
    $most_recent = array();
    foreach ($all_basemaps as $map) {
        // remember they're already sorted by city then date (descending)
        if (! array_key_exists( $map[ 'name' ], $most_recent )) {
            $most_recent[ $map[ 'name' ] ] = $map;
        }
    }
    
    return $most_recent;
}

function mkPinSelect( $setid ) {
    global $pin_colors;

    return '<select id="' . $setid . '_clr" onchange="updateImage( \'' . $setid . '\' );">' .
           implode( array_amap(
               function( $clr, $name ) {
                   return '<option value="' . $clr . '">' . $name . ' pin</option>';
               },
               $pin_colors
           ) ) .
           '</select>';
}

function mkPortalSetSelection( $mapid, $portalset ) {
    $captionmap = array(
        'captured'  => 'captured',
        'visited'   => 'visited but uncaptured',
        'unvisited' => 'unvisited'
    );

    $caption = 'Include ' . $captionmap[ $portalset ] . ' portals';

    $setid = $mapid . '_' . $portalset;
    $pinid = $setid . '_pin';
    $pinimg = $setid . '_img';
    return '<div class="mapset">' . NL .
           '    <input type="checkbox" class="mapsetselect" id="' . $setid . '" onclick="validateMapSelection( \'' . $mapid . '\' );">' . NL .
           '    <label for="' . $setid . '">' . $caption . '</label>' . NL .
           '    <div class="pinselect" id="' . $pinid . '">' . NL .
           '        <label for="' . $setid . '_clr">using</label>:'. NL .
           '        ' . mkPinSelect( $setid ) . NL .
           '        <img id="' . $pinimg . '" src="' . PIN_IMAGE_FOLDER . 'red_pin.png" class="pinimage">' . NL .
           '    </div>' . NL .
           '</div>';
}

function mkUploadForm() {
    return '<div class="indented underlined">' . NL .
           '    <form enctype="multipart/form-data" action="/' . ROOT_FOLDER . '" method="POST">' . NL .
           '        <input type="hidden" name="action" value="upload">' . NL .
           '        <input type="hidden" name="MAX_FILE_SIZE" value="' . MAX_UPLOAD_SIZE . '">' . NL .
           '        <label for="agent_name">Agent name</label>: @<input id="agent_name" name="agent_name" type="text" value=""><br><br>' . NL .
           '        <label for="game_log">Game log (this is the file &#x201c;<code>game_log.tsv</code>&#x201d; from your personal data dump)</label>:<br>' . NL .
           '        <input id="game_log" name="game_log" type="file" class="underlined">' . NL .
           '        <span class="annot">max file size: ' . number_format( (MAX_UPLOAD_SIZE / 1048576), 1 ) . ' MB</span>' . NL .
           '        <br><br>' . NL .
           '        <input type="submit" value="upload log">' . NL .
           '        <span>(This may take a while depending on the size of your game log.)</span>' . NL .
           '    </form>' . NL .
           '</div>' . NL;
}

function indent( $str, $n = 1 ) {
    $prefix = str_repeat( '    ', $n );
    $lines = explode( NL, $str );
    return $prefix . implode( (NL . $prefix), $lines );
}

function mkFAQ() {
    global $KNOWN_LOCATIONS;
    $knownlocations = implode( ', ', $KNOWN_LOCATIONS );

    $faq = array(
        'whatis' => array(
            'q' => 'What is this?',
            'a' => '<p>This is a utility to generate personal portal maps for an <a href="https://www.ingress.com/">Ingress</a> agent.</p>' . NL .
                   '<p>This is done by reading that agent&#x2019;s data dump from Niantic and correlating it with lists of known portals.' . NL .
                   'These personal maps may include combinations of captured portals, visited (but not yet captured) portals, and' . NL .
                   'unvisited portals.</p>' . NL .
                   '<p>An agent may use it to do things like:' . NL .
                   '<ul>' . NL .
                   '    <li>Determine where in their hometown<sup>*</sup> they still have unvisited and/or uncaptured unique portals;</li>' . NL .
                   '    <li>Get driving directions and do other planning for ops (conveniently rendered in mobile offline apps);</li>' . NL .
                   '    <li>Look at all the places in the world that they&#x2019;ve played (cool for reminiscing).</li>' . NL .
                   '</ul></p>' . NL .
                   '<p><sup>*</sup> <span class="annot">Currently supported locations (i.e.: &#x201c;lists of known portals&#x201d;) on' . NL .
                   'this server include:<br> &nbsp; [' . $knownlocations . ']</span></p>'
        ),
        'howto' => array(
            'q' => 'How do I get my own map?',
            'a' => '<p>First, you need to get your personal data dump from Niantic. There&#x2019;s no automatic process for doing this;' . NL .
                   'what you will need to do is send them an email at &nbsp;<tt>privacy&#x24d0;nianticlabs.com</tt> requesting your data.' . NL .
                   'The email can be really simple; here&#x2019;s <a href="' . UI_IMAGE_FOLDER . 'ss_nia_email.png" target="_blank">a' . NL .
                   'screenshot of an example</a>.</p>' . NL .
                   '<p><span class="sc">[Important!]</span> Once you have sent the email, make sure you receive a response back from' . NL .
                   'Niantic stating that the &#x201c;request has been transferred to the appropriate team for processing&#x201d; (or words' . NL .
                   'to that effect); this confirms that your request is in the system. After receiving this response, it will take about' . NL .
                   '3-4 weeks for them to process the request and give you your data.</p>' . NL .
                   '<p>Finally, you should receive a second email with a link to your file. Download the link and use the provided password' . NL .
                   'to unzip it. Inside will be a couple dozen files; one of which is called &nbsp;<tt>game_log.tsv</tt>. Return to this' . NL .
                   'page, and use the form above to upload that file<sup>*</sup>. You will then be presented with a set of options to' . NL .
                   'customize, preview and download your personal maps in <a href="https://en.wikipedia.org/wiki/Keyhole_Markup_Language">KML</a>' . NL .
                   'format.</p>' . NL .
                   '<p><sup>*</sup> <span class="annot">Note this <span class="ul">will</span> involve you uploading your personal log to' . NL .
                   'this server. Will I have the ability to look at it? Technically yes, since it&#x2019;s my server. I promise that I' . NL .
                   'won&#x2019;t ever do that without your explicit permission; it&#x2019;s up to you to believe me or not. Unfortunately' . NL .
                   'there&#x2019;s no easy way around this requirement, short of you downloading the code and running it yourself on your' . NL .
                   'own computer. If you&#x2019;re technically proficient and interested in doing that, I&#x2019;ve made the code available' . NL .
                   '(see question below).</span></p>'
        ),
        'kml' => array(
            'q' => 'Ok, I downloaded the KML; now what?',
            'a' => '<ul>' . NL .
                   '    <li><span class="li_head">On desktop:</span><br>' . NL .
                   '        Install <a href="https://www.google.com/earth/">Google Earth</a>' . NL .
                   '        to view your personal portal maps.<br>' . NL .
                   '        <img src="' . UI_IMAGE_FOLDER . 'ss_google_earth.png" onclick="showImgFullsize( this );" class="thumb"><br>' . NL .
                   '        <span class="image-caption">(click to see full-size)</span></li>' . NL .
                   '    <li><span class="li_head">On mobile:</span><br>' . NL .
                   '        Install <a href="https://maps.me/">maps.me</a>' . NL .
                   '        to view your personal portal maps (import as bookmarks).<br>' . NL .
                   '        <img src="' . UI_IMAGE_FOLDER . 'ss_maps_me.png" onclick="showImgFullsize( this );" class="thumb"><br>' . NL .
                   '        <span class="image-caption">(click to see full-size)</span></li>' . NL .
                   '    <li><span class="li_head">On IITC:</span><br>' . NL .
                   '        Install the <a href="https://static.iitc.me/build/release/plugins/add-kml.user.js">&#x201c;overlay kml&#x201d; IITC plugin</a>' . NL .
                   '        to add your personal portal maps.<br>' . NL .
                   '        <img src="' . UI_IMAGE_FOLDER . 'ss_iitc.png" onclick="showImgFullsize( this );" class="thumb"><br>' . NL .
                   '        <span class="image-caption">(click to see full-size)</span></li>' . NL .
                   '    <li><span class="li_head">On your GPS unit:</span><br>' . NL .
                   '        Refer to your GPS unit&#x2019;s documentation to see if and how it can import KML.<br>' . NL .
                   '        If your GPS doesn&#x2019;t mention KML anywhere, you can use <a href="http://www.gpsvisualizer.com/">GPS Visualizer</a>' . NL .
                   '        to convert the KML into a format that your GPS may be able to use.</li>' . NL .
                   '    <li><span class="li_head">Other options:</span><br>' . NL .
                   '        Almost any GIS program will open KML files (although if you&#x2019;re familiar with GIS you' . NL .
                   '        probably already know that).<br>' . NL .
                   '        See also the following pages for an idea of other software that can read KML:<br>' . NL .
                   '        &nbsp; [<a href="https://fileinfo.com/extension/kml">fileinfo.com page</a>]<br>' . NL .
                   '        &nbsp; [<a href="https://askubuntu.com/questions/18520/where-can-i-get-an-application-to-work-with-my-kml-files">askubuntu.com discussion</a>]</li>' . NL .
                   '</ul>'
        ),
        'source' => array(
            'q' => 'Can I get my hands on the source code for this?',
            'a' => '<p>You bet! It&#x2019;s available in a <a href="https://bitbucket.org/sean_nichols/portal-db/">git repository at BitBucket</a>' . NL .
                   '(requires Python 3, and optionally PHP 5, to run).</p>'
        ),
        'allportals' => array(
            'q' => 'Can I just see all the portals in a city, without making a personalized map?',
            'a' => '<p>Yep! Here are the city maps currently available on this server:</p>' . NL .
                   '<ul>' . NL .
                   implode( array_map(
                       function( $map ) {
                           return '    <li><a href="/' . ROOT_FOLDER . '?kml=' . $map[ 'file' ] . '" download>' . $map[ 'label' ] . '</a></li>' . NL;
                       },
                       mostRecentBasemaps( $KNOWN_LOCATIONS )
                   ) ) . NL .
                   '</ul>'
        ),
        'whoami' => array(
            'q' => 'Um, wait. Who are you again?',
            'a' => '<img src="' . UI_IMAGE_FOLDER . 'profile_sirputter.png" class="profile-image">' . NL .
                   '<p class="contact">The author of this utility is Sean Nichols, known in-game as &nbsp;' . NL .
                   '<span style="font-style: normal;">@sirputter</span>. You can also reach' . NL .
                   'me at <tt>seanni&#x24d0;trichotomy.ca</tt>.</p>' . NL .
                   '<p>The rest of the not-very-exciting content on my server is <a href="http://trichotomy.ca/sitemap.php">here</a>.</p>'
        )
    );
    
    $output = '<dl class="faq indented underlined">' . NL;
    foreach ($faq as $id => $entry) {
        $output .= '    <dt id="faq_' . $id . '_q" class="hidden" onclick="showFAQ( this );"><a name="faq_' . $id . '">' . $entry[ 'q' ] . '</a></dt>' . NL .
                   '    <dd id="faq_' . $id . '_a" class="hidden">' . NL .
                   indent( $entry[ 'a' ], 2 ) . NL .
                   '    </dd>' . NL;
    }
    $output .= '</dl>' . NL;
    
    return $output;
}

function mkIndexPage() {
    $pagename = 'Create profile';
    $pagehead = '';
    $output   = '<b>Upload your game log to create profile and generate maps:</b><br><br>' . NL . NL .
                mkUploadForm() . NL .
                '<br>' . NL .
                '<b>Q & A:</b>' . NL .
                mkFAQ();

    return array( $pagename, $pagehead, $output );
}

function mkProfilePage( $pid ) {
    global $KNOWN_LOCATIONS;

    $profile = readProfile( $pid ) ;
    
    $pagename = 'Personal maps for @' . $profile[ 'agent' ];
    
    $pagehead = '    <link rel="stylesheet" href="ol.css" type="text/css">' . NL .
                '    <script src="ol.js"></script>' . NL;

    $city_basemaps = listBasemaps( $KNOWN_LOCATIONS );
    $city_basemap_options = array_map(
        function( $m ) {
            return '<option value="' . $m[ 'file' ] . '">' . $m[ 'label' ] . '</option>';
        },
        $city_basemaps
    );

    $thisurl = SERVER_HOSTNAME . ROOT_FOLDER . '?' . $pid;
    $pagelink = '<p>' . NL .
                '    To access this page again in the future, please save or bookmark the link:' . NL .
                '    <a href="' . $thisurl . '">' . $thisurl . '</a>' . NL .
                '</p>' . NL;
    $cityform = '<h3>Create map of City portals</h3>' . NL .
                '<div class="indented underlined">' . NL .
                '    <label for="city_base">Select city base map to compare against</label>:' . NL .
                '    <select id="city_base" name="city_base">' . NL .
                '        ' . implode( '', $city_basemap_options ) . NL .
                '    </select><br>' . NL .
                mkPortalSetSelection( 'city', 'unvisited' ) . NL .
                mkPortalSetSelection( 'city', 'visited' ) . NL .
                mkPortalSetSelection( 'city', 'captured' ) . NL .
                '    <form id="city_mapreq" action="/' . ROOT_FOLDER . '" method="POST">' . NL .
                '        <input type="hidden" name="' . $pid . '" value="">' . NL .
                '        <input type="hidden" id="city_map" name="map" value="">' . NL .
                '        <input type="hidden" id="city_baseid" name="base" value="">' . NL .
                '        <button id="city_submit" onclick="submitMapRequest( \'city\' );" type="button" disabled>Download KML</button>' . NL .
                '        <button id="city_preview" onclick="previewMapRequest( \'city\' );" type="button" disabled>Preview on map &darr;</button>' . NL .
                '    </form>' . NL .
                '</div>' . NL;

    $worldform = '<h3>Create map of Worldwide portals</h3>' . NL .
                 '<div class="indented underlined">' . NL .
                 mkPortalSetSelection( 'world', 'visited' ) . NL .
                 mkPortalSetSelection( 'world', 'captured' ) . NL .
                 '    <form id="world_mapreq" action="/' . ROOT_FOLDER . '" method="POST">' . NL .
                 '        <input type="hidden" name="' . $pid . '" value="">' . NL .
                 '        <input type="hidden" id="world_map" name="world" value="">' . NL .
                 '        <button id="world_submit" onclick="submitMapRequest( \'world\' );" type="button" disabled>Download KML</button>' . NL .
                 '        <button id="world_preview" onclick="previewMapRequest( \'world\' );" type="button" disabled>Preview on map &darr;</button>' . NL .
                 '    </form>' . NL .
                 '</div>' . NL;

    $previewmap = '<div id="ol_map" class="indented ol_map"></div>' . NL;
    
    $output = $pagelink .
              $cityform .
              $worldform .
              $previewmap;

    return array( $pagename, $pagehead, $output );
}

function mkPage_( $arr, $error = NULL ) {
    list( $pagename, $pagehead, $output ) = $arr;
    return mkPage( $pagename, $pagehead, $output, $error );
}

function mkPage( $pagename, $pagehead, $output, $error = NULL ) {
    global $KNOWN_LOCATIONS;
    $knownlocations = implode( ', ', $KNOWN_LOCATIONS );

    $title = 'Ingress Portal Maps: ' . $pagename;

    if (is_null( $error )) {
        $error = '';
    } else {
        $error = '    <div class="errormsg"><p>ERROR: ' . $error . '</p></div>' . NL;
    }
    
    $description = 'A utility to parse an agent\'s personal data dump and display a map of remaining unvisited or uncaptured portals, ' .
                   'as well as places they\'ve been so far.';

    $home_folder = SERVER_HOSTNAME . ROOT_FOLDER;
    $image_folder = $home_folder . UI_IMAGE_FOLDER;
    
    return '<!DOCTYPE html>' . NL .
           '<html lang="en">' . NL .
           '<head>' . NL .
           '    <title>' . $title . '</title>' . NL .
           '    <meta charset="utf-8">' . NL .
           '    <meta name="author" content="' . AUTHOR_NAME . ' @' . AUTHOR_AGENT_NAME . '">' . NL .
           '    <meta name="title" content="' . $title . '">' . NL .
           '    <meta name="keywords" content="Ingress, portals, map, personal data, ' .
                    'KML, unique visits, unique captures, ' . $knownlocations . '">' . NL .
           '    <meta name="description" content="' . $description . '">' . NL .
           '    <meta property="og:title" content="' . $title . '">' . NL .
           '    <meta property="og:url" content="' . $home_folder . '">' . NL .
           '    <meta property="og:description" content="' . $description . '">' . NL .
           '    <meta property="og:site_name" content="Portal Maps">' . NL .
           '    <meta property="og:image" content="' . $image_folder .'preview.png">' . NL .
           '    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Coda">' . NL .
           '    <link rel="stylesheet" href="portal_map.css">' . NL .
           '    <link rel="stylesheet" href="' . UI_IMAGE_FOLDER . 'portal_map_images.css">' . NL .
           $pagehead .
           '    <script type="text/javascript" src="portal_map.js"></script>' . NL .
           '    <script type="text/javascript">' . NL .
           'const PIN_IMAGE_FOLDER = \'' . PIN_IMAGE_FOLDER . '\';' . NL .
           'window.onload = init;' . NL .
           '    </script>' . NL .
           '</head>' . NL .
           '<body>' . NL .
           $error .
           '    <h2>' . $title . '</h2>' . NL .
           $output .
           '</body>' . NL .
           '</html>';
}

function coordsToPortals( $coord_lst ) {
    return array_map(
        function( $p ) {
            list( $lat, $long ) = explode( ',', $p );
            return compact( 'lat', 'long' );
        },
        $coord_lst
    );
}

function getMapset( $mapset, $agent_portals, $base_portals = NULL ) {
    $visited   = $agent_portals[ 'visited' ];
    $captured  = $agent_portals[ 'captured' ];
    
    $plist = array();
    switch ($mapset) {
        case 'captured':
            if (is_null( $base_portals )) {
                $plist = coordsToPortals( $captured );
                break;
            }
            foreach ($base_portals as $portal) {
                $latlong = $portal[ 'lat' ] . ',' . $portal[ 'long' ];
                if (in_array( $latlong, $captured )) {
                    $plist[] = $portal;
                }
            }
            break;
        case 'unvisited':
            if (is_null( $base_portals )) {
                // nuthin' to do here
                break;
            }

            $agent_all = array_merge( $visited, $captured );
            foreach ($base_portals as $portal) {
                $latlong = $portal[ 'lat' ] . ',' . $portal[ 'long' ];
                if (! in_array( $latlong, $agent_all)) {
                    $plist[] = $portal;
                }
            }
            break;
        case 'visited':
            if (is_null( $base_portals )) {
                $plist = coordsToPortals( $visited );
                break;
            }
            foreach ($base_portals as $portal) {
                $latlong = $portal[ 'lat' ] . ',' . $portal[ 'long' ];
                if (in_array( $latlong, $visited )) {
                    $plist[] = $portal;
                }
            }
            break;
    }
    
    return $plist;
}

function mkKMLStyle( $clr, $embed = false ) {
    global $pin_colors;

    $pin_folder = SERVER_HOSTNAME . ROOT_FOLDER . PIN_IMAGE_FOLDER;

    if ($embed) {
        $pintype = '_dot';
        $hotspot = array( 4, 4 );
    } else {
        $pintype = '_lrg';
        $hotspot = array( 32, 6 );
    }
    $styles = array(
        'normal' => array(
            'Icon' => array( 'Icon',     $pintype,  true ),
            'List' => array( 'ItemIcon', '_bullet', false )
        ),
        'highlight' => array(
            'Icon' => array( 'Icon',     $pintype,  true ),
            'List' => NULL
        )
    );

    $color = $pin_colors[ $clr ];
    $stylekml = '';
    $hotspot_tag = '<hotSpot x="' . $hotspot[ 0 ] . '" y="' . $hotspot[ 1 ] . '" xunits="pixels" yunits="pixels"/>';
    foreach ($styles as $key => $styledata) {
        $stylekml .= '    <Style id="placemark-' . $color . '-' . $key . '">' . NL;
        foreach ($styledata as $stype => $sdata) {
            if ($sdata) {
                list( $element, $suffix, $hotspot ) = $sdata;
                $pinurl = $pin_folder . $clr . $suffix;
                $stylekml .= '        <' . $stype .'Style>' . NL .
                             '            <' . $element . '>' . NL .
                             '                <href>' . $pinurl . '.png</href>' . NL .
                             '            </' . $element . '>' . NL;
                if ($hotspot) {
                    $stylekml .= '            ' . $hotspot_tag . NL;
                }
                $stylekml .= '        </' . $stype . 'Style>' . NL;
            }
        }
        $stylekml .= '        <LabelStyle>' . NL .
                     '            <color>00ffffff</color>' . NL .
                     '        </LabelStyle>' . NL .
                     '    </Style>' . NL;
    }
    $stylekml .= '    <StyleMap id="placemark-' . $color .'">' . NL;
    foreach (array_keys( $styles ) as $key) {
        $stylekml .= '        <Pair>' . NL .
                     '            <key>' . $key . '</key>' . NL .
                     '            <styleUrl>#placemark-' . $color . '-' . $key . '</styleUrl>' . NL .
                     '        </Pair>' . NL;
    }
    $stylekml .= '    </StyleMap>' . NL;

    return $stylekml;
}

function parseMapset( $set, $mapdef ) {
    global $pin_colors;

    $idx = $set * 2;
    $include_set = (bool) ($mapdef[ $idx ]);
    $clr = array_keys( $pin_colors )[ (int) ($mapdef[ $idx + 1 ]) ];

    return array( $include_set, $clr );
}

function locationWords( $basename ) {
    $name_prefixes = array(
        'all'
    );
    
    $words = explode( '_', $basename );
    array_shift( $words );
    if (in_array( strtolower( reset( $words ) ), $name_prefixes )) {
        array_shift( $words );
    }
    if (end( $words ) == 'portals') {
        array_pop( $words );
    }
    
    return $words;
}

function mkTaggedData( $tag, $data, $indent = 0, $nl = false, $attr = NULL ) {
    $prefix = str_repeat( '    ', $indent );
    if ($nl) {
        $data = NL . $prefix . '    ' . $data . NL;
    }
    $attr = (is_null( $attr ) ? '' : ' ' . $attr);

    return $prefix . '<' . $tag . $attr . '>' . $data . '</' . $tag . '>' . NL;
}

function mkKMLPlacemark_( $portal, $color, $embed = false ) {
    $name  = (array_key_exists( 'name', $portal ) ? htmlspecialchars( $portal[ 'name' ] ) : NULL);
    $added = (array_key_exists( 'added', $portal ) ? $portal[ 'added' ] : NULL);
    $img   = (array_key_exists( 'image', $portal ) ? $portal[ 'image' ] : NULL);
    
    return mkKMLPlacemark( $portal[ 'lat' ], $portal[ 'long' ], $color, $name, $added, $img, $embed );
}

function mkKMLPlacemark( $lat, $long, $color, $name = NULL, $added = NULL, $img = NULL, $embed = false ) {
    $nametag = ((is_null( $name ) || $embed) ? '' : mkTaggedData( 'name', $name, 3 ));
    $latlong_desc = '$[latlong/displayName]: <i>$[latlong]</i><br>';
    if (is_null( $added )) {
        $added_data = '';
        $added_desc = '';
    } else {
        $addcontents = NL .
                       mkTaggedData( 'displayName', 'Imported', 5 ) . NL .
                       mkTaggedData( 'value', $added, 5 ) . NL;
        $added_data = mkTaggedData( 'Data', $addcontents, 4, false, 'name="added"' );
        $added_desc = '<br>$[added/displayName]: <i>$[added]</i>';
    }
    if (is_null( $img )) {
        $img_data = '';
        $img_desc = '';
    } else {
        $imgcontents = NL .
                       mkTaggedData( 'displayName', 'Image', 5 ) . NL .
                       mkTaggedData( 'value', $img, 5 ) . NL;
        $img_data = mkTaggedData( 'Data', $imgcontents, 4, false, 'name="image"' );
        $img_desc = '<br>' .
                    '<a href="' . $img . '">' .
                    '<img src="' . $img . ' style="width: 250px;">' .
                    '</a>';
    }

    $description = $latlong_desc . $added_desc . $img_desc;
    
    return '        <Placemark>' . NL .
           $nametag .
           '            <visibility>1</visibility>' . NL .
           '            <description><![CDATA[' . $description . ']]></description>' . NL .
           '            <snippet></snippet>' . NL .
           '            <styleUrl>#placemark-' . $color . '</styleUrl>' . NL .
           '            <Point>' . NL .
           '                <coordinates>' . $long . ',' . $lat . ',0</coordinates>' . NL .
           '            </Point>' . NL .
           '            <ExtendedData>' . NL .
           '                <Data name="latlong">' . NL .
           '                    <displayName>Coordinates</displayName>' . NL .
           '                    <value>(' . $lat . ', ' . $long . ')</value>' . NL .
           '                </Data>' . NL .
           $added_data .
           $img_data .
           '            </ExtendedData>' . NL .
           '        </Placemark>' . NL;
}

function mkBaseKML( $data ) {
    global $pin_colors;
    
    $basename = $data[ 'file' ];
    $basemap = readBasemap( $basename );
    $base_ts = $basemap[ 'current' ];
    $base_portals = getBasemapPortalList( $basemap[ 'portals' ] );
    $clr = 'red';
    $embed = false;
    
    $loc_words = locationWords( $basename );
    
    $metadata = array(
        'filename' => implode( '_', $loc_words ) . '_portals_map.kml',
        'folder'   => implode( ' ', $loc_words ) . ' portals',
        'current'  => $base_ts
    );
    
    $pinstyles = mkKMLStyle( $clr, $embed );
    $mapdata = '';
    $color = $pin_colors[ $clr ];
    foreach ($base_portals as $portal) {
        $mapdata .= mkKMLPlacemark_( $portal, $color, $embed );
    }

    return array( $metadata, $pinstyles, $mapdata );
}

function mkLocalKML( $data ) {
    global $pin_colors;

    $profile = readProfile( $data[ 'pid' ] );
    $basename = $data[ 'base' ];
    $basemap = readBasemap( $basename );
    $embed   = $data[ 'embed' ];

    $agentname    = $profile[ 'agent' ];
    $portal_log   = $profile[ 'log' ][ 'portals' ];
    $base_ts      = $basemap[ 'current' ];
    $base_portals = getBasemapPortalList( $basemap[ 'portals' ] );

    $loc_words = locationWords( $basename );
    
    $metadata = array(
        'filename' => implode( '_', $loc_words ) . '_portals_map_for_' . $agentname . '.kml',
        'folder'   => implode( ' ', $loc_words ) . ' portals for ' . $agentname,
        'current'  => $base_ts
    );

    $pinstyles = '';
    $mapdata = '';
    $mapdef = str_split( $data[ 'map' ] );

    $mapsets = array( 'unvisited', 'visited', 'captured' );
    $colors_used = array();
    for ($i = 0; $i < count( $mapsets ); $i++) {
        list( $include_set, $clr ) = parseMapset( $i, $mapdef );
        if ($include_set) {
            if (! in_array( $clr, $colors_used )) {
                $colors_used[] = $clr;
                $pinstyles .= mkKMLStyle( $clr, $embed );
            }

            $color = $pin_colors[ $clr ];
            $mapset_data = getMapset( $mapsets[ $i ], $portal_log, $base_portals );
            foreach ($mapset_data as $portal) {
                $mapdata .= mkKMLPlacemark_( $portal, $color, $embed );
            }
        }
    }

    return array( $metadata, $pinstyles, $mapdata );
}

function mkWorldKML( $data ) {
    global $pin_colors;

    $profile = readProfile( $data[ 'pid' ] );

    $agentname  = $profile[ 'agent' ];
    $portal_log = $profile[ 'log' ][ 'portals' ];

    $metadata = array(
        'filename' => 'portal_history_map_for_' . $agentname . '.kml',
        'folder'   => 'Portal history for ' . $agentname,
        'current'  => NULL
    );

    $mapsets = array( 'visited', 'captured' );

    $pinstyles = '';
    $mapdata = '';
    $mapdef = str_split( $data[ 'map' ] );
    $embed = $data[ 'embed' ];
    
    $mapsets = array( 'unvisited', 'visited', 'captured' );
    $colors_used = array();
    for ($i = 0; $i < count( $mapsets ); $i++) {
        list( $include_set, $clr ) = parseMapset( $i, $mapdef );
        if ($include_set) {
            if (! in_array( $clr, $colors_used )) {
                $colors_used[] = $clr;
                $pinstyles .= mkKMLStyle( $clr, $embed );
            }

            $color = $pin_colors[ $clr ];
            $mapset_data = getMapset( $mapsets[ $i ], $portal_log );
            foreach ($mapset_data as $portal) {
                $mapdata .= mkKMLPlacemark_( $portal, $color, $embed );
            }
        }
    }

    return array( $metadata, $pinstyles, $mapdata );
}

function mkKML_( $arr, $embed = false ) {
    list( $metadata, $pinstyles, $mapdata ) = $arr;
    return mkKML( $metadata, $pinstyles, $mapdata, $embed );
}

function mkKML( $metadata, $pinstyles, $mapdata, $embed = false ) {
    $filename = $metadata[ 'filename' ];
    $now = date( DATE_FORMAT );
    if ($metadata[ 'current' ]) {
        $current = '    Information understood to be current as of ' .
                   date( DATE_FORMAT, $metadata[ 'current' ] ) . NL;
    } else {
        $current = '';
    }

    $header =    '<?xml version="1.0" encoding="UTF-8"?>' . NL .
                 '<!--' . NL .
                 '    KML generated by /Simple Ingress portal database manager v0.2.1/' . NL .
                 '    Contact: Sean Nichols seanni@trichotomy.ca / Ingress: @sirputter' . NL .
                 '    See also: http://www.trichotomy.ca/portals' . NL . NL .
                 '    KML generated ' . $now . NL .
                 $current .
                 '-->' . NL .
                 '<kml xmlns="http://www.opengis.net/kml/2.2"' . NL .
                 '     xmlns:gx="http://www.google.com/kml/ext/2.2"' . NL .
                 '     xmlns:kml="http://www.opengis.net/kml/2.2"' . NL .
                 '     xmlns:atom="http://www.w3.org/2005/Atom">' . NL .
                 '<Document>' . NL .
                 '    <name>' . $metadata[ 'folder' ] . '</name>' . NL;
    $folderdef = '    <Folder>' . NL .
                 '        <name>' . $metadata[ 'folder' ] . '</name>' . NL .
                 '        <visibility>1</visibility>' . NL;
    $footer =    '    </Folder>' . NL .
                 '</Document>' . NL .
                 '</kml>' . NL;

    $kml = $header .
           $pinstyles .
           $folderdef .
           $mapdata .
           $footer;

    header( 'Content-Type: application/vnd.google-earth.kml+xml' );
    if (! $embed) {
        header( 'Content-Disposition: attachment; filename="' . $filename . '"' );
        header( 'Content-Description: File Transfer' );
    }
    header( 'Content-Length: ' . strlen( $kml ) );

    return $kml;
}

function bool_param( $param, $to_check = NULL ) {
    if (is_null( $to_check )) {
        $to_check = array_merge( $_GET, $_POST );
    }

    if (array_key_exists( $param, $to_check )) {
        $true_vals = array( 't', 'y', 'true', 'yes' );
        return in_array( strtolower( $to_check[ $param ] ), $true_vals );
    }
    return false;
}

function choose_action() {
    if ($_POST[ 'action' ] == 'upload') {
        $action = 'upload';
        $extra = array(
            'agent' => $_POST[ 'agent_name' ],
            'log'   => $_FILES[ 'game_log' ]
        );
    } elseif (array_key_exists( 'kml', $_POST )) {
        $action = 'basemap';
        $extra = array(
            'file' => $_POST[ 'kml' ]
        );
    } elseif (array_key_exists( 'kml', $_GET )) {
        $action = 'basemap';
        $extra = array(
            'file' => $_GET[ 'kml' ]
        );
    } else {
        $pid = hasProfile();
        if ($pid) {
            if ($_POST[ 'map' ]) {
                $action = 'citymap';
                $extra = array(
                    'pid'   => $pid,
                    'map'   => $_POST[ 'map' ],
                    'base'  => $_POST[ 'base' ],
                    'embed' => bool_param( 'embed', $_POST )
                );
            } elseif ($_GET[ 'map' ]) {
                $action = 'citymap';
                $extra = array(
                    'pid'   => $pid,
                    'map'   => $_GET[ 'map' ],
                    'base'  => $_GET[ 'base' ],
                    'embed' => bool_param( 'embed', $_GET )
                );
            } elseif ($_POST[ 'world' ]) {
                $action = 'worldmap';
                $extra = array(
                    'pid'   => $pid,
                    'map'   => $_POST[ 'world' ],
                    'embed' => bool_param( 'embed', $_POST )
                );
            } elseif ($_GET[ 'world' ]) {
                $action = 'worldmap';
                $extra = array(
                    'pid'   => $pid,
                    'map'   => $_GET[ 'world' ],
                    'embed' => bool_param( 'embed', $_GET )
                );
            } else {
                $action = 'profile';
                $extra = $pid;
            }
        } else {
            $action = 'index';
            $extra = NULL;
        }
    }

    return compact( 'action', 'extra' );
}

function main() {
    date_default_timezone_set( HOME_TIMEZONE );

    $do = choose_action();

    switch ($do[ 'action' ]) {
        case 'citymap':
            $embed = $do[ 'extra' ][ 'embed' ];
            $response = mkKML_( mkLocalKML( $do[ 'extra' ] ), $embed );
            break;
        case 'worldmap':
            $embed = $do[ 'extra' ][ 'embed' ];
            $response = mkKML_( mkWorldKML( $do[ 'extra' ] ), $embed );
            break;
        case 'basemap':
            $response = mkKML_( mkBaseKML( $do[ 'extra' ] ) );
            break;
        case 'upload':
            $response = profileUploaded( $do[ 'extra' ] );
            break;
        case 'profile':
            $response = mkPage_( mkProfilePage( $do[ 'extra' ] ) );
            break;
        case 'index':
            $response = mkPage_( mkIndexPage() );
            break;
    }

    echo $response;
}

main();

?>

<?php

// SERVER_HOSTNAME -- definitely change me!!
//  All others in this section - optional
define( 'SERVER_HOSTNAME',   'http://trichotomy.ca/' ); // Change me!
define( 'ROOT_FOLDER',       'portals/' );
define( 'UI_IMAGE_FOLDER',   'images/' );
define( 'PIN_IMAGE_FOLDER',  'images/' );
define( 'PROFILES_FOLDER',   'profiles/' );
define( 'BASEMAPS_FOLDER',   'basemaps/' );

// These just get inserted into the html headers; you can leave them if you want
define( 'AUTHOR_NAME',       'Sean Nichols' );
define( 'AUTHOR_AGENT_NAME', 'sirputter' );

// You probably want to change the time zone.
// The max upload size is up to you but probably don't set it much smaller than
//  50MB; as that's a not-unreasonable size for agents' game logs to reach.
define( 'HOME_TIMEZONE',     'America/Edmonton' );
define( 'MAX_UPLOAD_SIZE',   52428800 );

// *** IMPORTANT ***
// You will need to ADD to this list every time you add a JSON (with
//  a new location) to the basemaps/ folder!!
// The SF ferry building is the one that comes with the examples, which
//  you use/generate if you follow the "Quick Start" section in the
//  README.md file. If you don't walk through the quick start, then you
//  should probably remove that entry from the list.
$KNOWN_LOCATIONS = array( 'Ferry Building' );

?>

// This is a bookmarklet, that is designed to work with IITC.
// Click a portal, then click the bookmarklet; it will grab the info and print it to the console log.
//  Then you can copy the contents of the console log and paste it into a new file which is the correct
//  format to import directly into python-db (using the "add" command).
// Note you will need to filter the console log by 'intel.ingress.com' to filter out all the other
//  messages logged by IITC (and whatever else you may have running); there's a lot in there that you
//  don't want (and that portal-db may barf on).

javascript:(function() {
    var details, aside;
    var pf_str, pf_open, pf_close, pf_coord, pf_name;
    var pname, plat, plong;
    var pstr, portalurl;
    var imgpreview, imgurl;
    var i, j;
    
    var intel_url = 'https://intel.ingress.com/intel?';

    if (! window.location.href.startsWith( intel_url )) {
        alert( 'You need to be on the Ingress Intel page.' );
        return;
    }
    
    details = document.getElementsByClassName( 'linkdetails' );
    if (details.length == 0) {
        alert( 'You need to have a portal selected.' );
        return;
    }
     
    details = details[ 0 ];
    detailsparser: {
        for (i = 0; i < details.childNodes.length; i++) {
            if (details.childNodes[ i ].nodeName.toLowerCase() == 'aside') {
                aside = details.childNodes[ i ];
                for (j = 0; j < aside.childNodes.length; j++) {
                    if (aside.childNodes[ j ].nodeName.toLowerCase() == 'a') {
                        if (aside.childNodes[ j ].getAttribute( 'title' ).startsWith( 'Link to alternative maps' )) {
                            pf_str = aside.childNodes[ j ].getAttribute( 'onclick' );
                            pf_open = pf_str.indexOf( '(' );
                            pf_close = pf_str.lastIndexOf( ')' );

                            pf_str = pf_str.substring( pf_open + 1, pf_close ).trim();
                            pf_coord = pf_str.split( ',', 2 );
                            plat = pf_coord[ 0 ];
                            plong = pf_coord[ 1 ];
                            
                            pf_open = pf_str.indexOf( '\'' );
                            pf_close = pf_str.lastIndexOf( '\'' );
                            pname = pf_str.substring( pf_open + 1, pf_close );
                            
                            pname = pname.replace( /\\'/g, '\'' ).replace( /\\"/g, '"' );
                            
                            break detailsparser;
                        }
                    }
                }
            }
        }
    }
    portalurl = intel_url + 'll=' + plat + ',' + plong + '&z=17&pll=' + plat + ',' + plong + ' ';

    imgpreview = document.getElementsByClassName( 'imgpreview' );
    if (imgpreview.length == 0) {
        imgurl = '';
    } else {
        imgpreview = imgpreview[ 0 ];
        imgparser: {
            for (i = 0; i < imgpreview.childNodes.length; i++) {
                if (imgpreview.childNodes[ i ].nodeName.toLowerCase() == 'img') {
                    imgurl = imgpreview.childNodes[ i ].getAttribute( 'src' ) + ' ';
                    if (imgurl.substring( 0, 2 ) == '//') {
                        imgurl = '!https:' + imgurl;
                    } else {
                        imgurl = '!' + imgurl;
                    }
                }
            }
        }
    }
    
    pstr = portalurl + imgurl + pname;

    console.log( pstr );
})();

# -*- coding: utf-8 -*-

# Simple Ingress portal database manager
# Copyright 2018-2019, Sean C. Nichols
#  seanni@trichotomy.ca
#  Ingress: @sirputter

# Hereby released into the public domain: feel free to abuse this code as you see fit!
#

# Optionally uses the S2Sphere library, found at: http://s2sphere.sidewalklabs.com/
# If the library is present, can use it to determine S2 cells of the portals, otherwise
#  this feature will not be enabled.
#

# Requires Python 3.4+


from __future__ import print_function

from argparse import ArgumentParser, FileType, RawDescriptionHelpFormatter
from collections import namedtuple
from datetime import datetime
from functools import total_ordering
from io import TextIOWrapper
from json import dumps as dump_json
from logging import (
    INFO as MSG_NORMAL, DEBUG as MSG_VERBOSE, WARNING as MSG_QUIET,
    info as msg_normal, debug as msg_verbose, warning as msg_quiet,
    basicConfig as configLogging
)
from math import radians, sin, cos, asin, sqrt
from os.path import abspath, dirname, join as joinpath
from re import compile as compile_rexp, escape as escape_rexp
from shelve import open as open_db
from sys import stderr, stdout, version_info
from textwrap import dedent, indent as indent_text

try:
    from s2sphere import RegionCoverer, LatLng, LatLngRect, Cell as S2Cell
    s2_enabled = True
except ImportError:
    (RegionCoverer, LatLng, LatLngRect, S2Cell) = [None] * 4
    s2_enabled = False


__version__      = "0.2.1"
__version_info__ = (0, 2, 1)

_INTELPAGE = 'https://intel.ingress.com/intel'
_regioncoverer = None

_REQUIRES_PYTHON = (3, 4)
if version_info <= _REQUIRES_PYTHON:
    print("Requires Python version at least {}.{}.".format(*_REQUIRES_PYTHON))
    exit(1)


# Careful re: the ordering -- portal ordering is predicated *entirely* on their coordinates, not
#  any of the other attributes. This includes, for example, the name. So if a portal gets renamed
#  and added again (but with the same coordinates) it is considered the same portal (just with a
#  different name). If you need to differentiate, you will have to case this out yourself.

@total_ordering
class Location(object):
    lat       = 0
    long      = 0

    _DEFAULTS = {}

    @classmethod
    def setdefaults(cls, defaults):
        cls._DEFAULTS.update(defaults)

    @classmethod
    def default(cls, attrib):
        return cls._DEFAULTS.get(attrib, getattr(cls, attrib, None))

    @classmethod
    def read(cls, s):
        raise NotImplementedError

    def indexed(self):
        return (self.coords, self)

    @property
    def valid(self):
        return bool(self.coords)

    @property
    def coords(self):
        if self.lat or self.long:
            return (self.lat, self.long)
        return None

    def s2cell(self, lvl):
        if not s2_enabled:
            return None

        rc = _regioncoverer or RegionCoverer()

        # Even setting max_cells to 1 still returns a buncha' cells no matter what - so we
        #  have to filter the list we get back. Shrug.
        rc.min_level = lvl
        rc.max_level = lvl
        rc.max_cells = 1

        pt = LatLng.from_degrees(*map(float, self.coords))
        cellids = [cid for cid in rc.get_covering(LatLngRect.from_point(pt))]

        try:
            return [cid.id() for cid in cellids if S2Cell(cid).contains(pt.to_point())][0]
        except IndexError:
            # huh
            msg_quiet("Cannot find level {} S2 cell containing {}.".format(lvl, self))
            return None

    def distanceTo(self, other):
        # Haversine formula
        # https://www.movable-type.co.uk/scripts/latlong.html

        R = 6371008.8 # https://en.wikipedia.org/wiki/Earth_radius#Mean_radius

        # TODO: This assumes the Earth is a "true" sphere, not a flattened geoid (as is
        #         actually the case). It won't make a huge difference, but some of the
        #         measurements may be out by a few metres. This is a thing that could
        #         stand to be fixed, if anyone cares.
        #       Of course, to do it properly, you'd have to throw out Haversine, and
        #         instead implement a more intricate (and hence more expensive) formula,
        #         e.g.: along the lines of Vincenty's formulae; this is part of the
        #         reason I haven't done it.

        c1 = self.coords
        c2 = other.coords
        if not (c1 and c2):
            return None

        if self == other:
            return 0

        (lat1, lon1) = map(radians, map(float, c1))
        (lat2, lon2) = map(radians, map(float, c2))

        delta_lat = lat2 - lat1
        delta_lon = lon2 - lon1

        a = (sin(delta_lat / 2) ** 2) + cos(lat1) * cos(lat2) * (sin(delta_lon / 2) ** 2)
        c = 2 * asin(sqrt(a))

        return R * c;

    def __lt__(self, other):
        if not self.valid:
            return other.valid

        return (self.coords < other.coords)

    def __eq__(self, other):
        if not self.valid:
            return not other.valid

        return (self.coords == other.coords)

    def __init__(self, lat, long, **attribs):
        self.lat       = lat
        self.long      = long

        for (attrname, attrib) in self._DEFAULTS.items():
            setattr(self, attrname, attrib)

        for (attrname, attrib) in attribs.items():
            if hasattr(self, attrname):
                setattr(self, attrname, attrib)


# We're going to assume that log entries will never change over time; and that we can thus
#  reasonably hash them (based on location). This means that they should essentially be treated
#  as immutable. If you want to create new log entries or mess around with existing ones, then
#  this... probably isn't the code set you want to use to do that. Make your own Log library :-)

class PDLogEntry(Location):
    time = None
    track = None
    trigger = None
    comments = None

    # Any of the following actions count as a visit. (So do a few others, for example capturing
    #  the portal, hacking an Aegis, etc. - but those are always duplicates (timestamp+lat+long)
    #  of other, more general actions (in those examples, deploying a resonator, and successfully
    #  performing a hack, respectively). So we can safely ignore them; this set constitutes a
    #  minimal required coverage.

    # TODO: Actually there's one outstanding question: does using a fracker count?
    #       Google doesn't seem to have any answers -- need to experiment. We know for sure
    #       (thanks to @NinthAnt's May 2019 "Toast the Town" challenge) that beacons do NOT
    #       count, so for now I'm assuming frackers also don't.
    _VISIT_ACTIONS = (
        'created link',
        'hacked enemy portal',
        'hacked friendly portal',
        'hacked neutral portal',
        'resonator deployed',
        'resonator upgraded'
    )

    _TIMEFORMAT = '%Y %m-%d %H:%M:%S'

    @staticmethod
    def parseNone(s):
        if not s:
            return None
        if s.strip() == 'None':
            return None
        return s.strip()

    @classmethod
    def read(cls, s):
        fields = s.split('\t')
        return cls(
            lat      = cls.parseNone(fields[1]),
            long     = cls.parseNone(fields[2]),
            time     = datetime.strptime(fields[0], cls._TIMEFORMAT),
            track    = fields[3],
            trigger  = cls.parseNone(fields[4]),
            comments = cls.parseNone(fields[5]) if len(fields) >= 6 else None
        )

    @property
    def visited(self):
        return (self.track in self._VISIT_ACTIONS and self.trigger == 'success')

    @property
    def captured(self):
        return (self.track == 'captured portal')

    @property
    def timestr(self):
        return self.time.strftime(self._TIMEFORMAT)

    def toPortal(self, name=None):
        return Portal(
            lat      = self.lat,
            long     = self.long,
            name     = name,
            captured = self.captured,
            visited  = self.visited
        )

    def __hash__(self):
        return hash(self.coords)

    def __str__(self):
        return "{0.timestr}\t{0.lat}\t{0.long}\t{0.track}\t{0.trigger}\t{0.comments}".format(self)

class Portal(Location):
    isvalid    = False
    name       = ''
    created    = None
    iconstyle  = 'red_stars'
    visible    = True
    showlabel  = True
    visited    = False
    captured   = False
    imageurl   = None
    neighbours = []

    Attrib    = namedtuple('Attrib', ('description', 'value'))

    xml_ents = {
        '\\&'  : '&amp;',
        '\\<'  : '&lt;',
        '\\>'  : '&gt;',
        '\\\'' : '&apos;',
        '\\"'  : '&quot;'
    }
    xml_pattern = compile_rexp('|'.join(xml_ents.keys()))

    @classmethod
    def invalid(cls):
        return cls(lat=0, long=0, name='', isvalid=False)

    @classmethod
    def read(cls, s):
        s = s.strip()

        if s.startswith('portal_adder_launcher.js'):
            (_, s) = s.split(' ', 1)

        if s.startswith('#') or not '&pll=' in s:
            return cls.invalid()

        visited = True
        captured = False
        while s[0] in ('!', '*'):
            (mods, s) = s.split(' ', 1)
            if '!' in mods:
                visited = False
            if '*' in mods:
                captured = True

        (url, name) = s.split(' ', 1)
        (lat, long) = (url.split('&pll=')[1]).split(',')[:2]

        if any([name.startswith(s) for s in ('!http://', '!https://', '!//')]):
            (imageurl, name) = name[1:].split(' ', 1)
        else:
            imageurl = None

        return cls(
            lat      = lat,
            long     = long,
            name     = name,
            imageurl = imageurl,
            isvalid  = True,
            visited  = visited,
            captured = captured
        )

    def asdict(self, fields=None, s2=None):
        return self.attribs(fields, s2)

    def toCsv(self, fields=None, s2=None):
        if fields:
            fields = fields.split(',')
        formatters = [
            ('latlong',   lambda crd: "{},{}".format(*crd)),
            ('name',      lambda nam: '"{}"'.format(nam)),
            ('visited',   lambda vis: str(int(vis))),
            ('captured',  lambda cap: str(int(cap))),
            ('image',     lambda img: '"{}"'.format(img) if img else ""),
            ('added',     lambda dat: dat),
            ('neighbour', lambda nbr: '"{}"'.format(nbr.name) if nbr else ""),
            ('url',       lambda url: '"{}"'.format(url))
        ]

        attribs = self.attribs(fields, s2, datefmt='%Y-%m-%d %H:%M:%S')
        columns = [fmt(attribs[field].value) for (field, fmt) in formatters if field in fields]
        columns += [attribs['s2_{}'.format(lvl)].value for lvl in (s2 or [])]

        return ",".join(columns)

    def toGeoJSON(self, fields=None, s2=None):
        # TODO
        raise NotImplementedError

    def toKml(self, indent=0, fields=None, s2=None):
        padding = '    ' * indent
        if fields:
            fields = fields.split(',')
        attribs = self.attribs(fields, s2, xml=True)
        extdata = '\n'.join([(
            '        <Data name="{0}">\n'
            '            <displayName>{1.description}</displayName>\n'
            '            <value>{1.value}</value>\n'
            '        </Data>'
        ).format(*ed) for ed in attribs.items()])
        description = '<br>'.join([
            '$[{0}/displayName]: <i>$[{0}]</i>'.format(attr)
            for attr in attribs
            if attr != 'image'
        ])
        if 'image' in attribs:
            description += (
                '<br><a href="{0}">'
                '<img src="{0}" style="width: 250px;">'
                '</a>'
            ).format(attribs['image'].value)

        return indent_text('\n'.join([
            '<Placemark>',
            '    <name>{0.xmlname}</name>',
            '    <visibility>{0.visibility}</visibility>',
            '    <description><![CDATA[{1}]]></description>',
            '    <snippet></snippet>',
            '    <styleUrl>#sm_{0.style}</styleUrl>',
            '    <Point>',
            '        <coordinates>{0.long},{0.lat},0</coordinates>',
            '    </Point>',
            '    <ExtendedData>',
            '{2}',
            '    </ExtendedData>',
            '</Placemark>'
        ]).format(self, description, extdata), padding)

    def update(self, newdata, proplist):
        for pname in proplist:
            if hasattr(self, pname) and hasattr(newdata, pname):
                setattr(self, pname, getattr(newdata, pname))

    def attribs(self, fields=None, s2=None, xml=False, datefmt='%Y-%m-%d @ %H:%M:%S'):
        # TODO: Rewrite this so that it only calculates requested attributes,
        #       rather than calculating all, and discarding unused ones

        def _attr(desc, val):
            return self.Attrib(description=desc, value=val)

        self_name = self.xmlname if xml else self.name

        attrs = {
            'latlong'   : _attr("Coordinates",       self.coords),
            'name'      : _attr("Name",              self_name),
            'visited'   : _attr("Visited",           self.visited),
            'captured'  : _attr("Captured",          self.captured),
            'image'     : _attr("Image",             self.imageurl),
            'added'     : _attr("Imported",          self.date(datefmt)),
            'neighbour' : _attr("Closest Neighbour", self.closestNeighbour()),
            'url'       : _attr("Intel URL",         self.url)
        }

        if s2:
            def _s2data(lvl):
                s2_attr = self.Attrib(
                    description = "Level {} s2 cell".format(lvl),
                    value       = self.s2cell(lvl)
                )
                return ('s2_{}'.format(lvl), s2_attr)

            attrs.update(dict([_s2data(lvl) for lvl in s2]))

        for a in list(attrs):
            if attrs[a].value is None:
                del attrs[a]

        if fields:
            for a in list(attrs):
                if a not in fields and not a.startswith('s2'):
                    del attrs[a]

        return attrs

    def closestNeighbour(self, n=1):
        if n==1:
            if not self.neighbours:
                return None
            return self.neighbours[0]
        return self.neighbours[:n]

    def date(self, datefmt=None):
        if datefmt:
            return self.created.strftime(datefmt)
        else:
            return self.created

    @property
    def valid(self):
        return self.isvalid and super(Portal, self).valid

    @property
    def xmlname(self):
        return self.xml_pattern.sub(lambda m: self.xml_ents[escape_rexp(m.group(0))], self.name)

    @property
    def visibility(self):
        return int(self.visible)

    @property
    def style(self):
        return self.iconstyle + ('' if self.showlabel else '_nolabel')

    @property
    def details(self):
        # TODO
        raise NotImplementedError

    @property
    def imageid(self):
        if not self.imageurl:
            return None
        return self.imageurl.split('/')[-1]

    @property
    def url(self):
        return '{0}?ll={1.lat},{1.long}&z=17&pll={1.lat},{1.long}'.format(_INTELPAGE, self)

    @property
    def imageurl_(self):
        if not self.imageurl:
            return None
        return "!{}".format(self.imageurl)

    @property
    def flags(self):
        return "{}{}".format("" if self.visited else "!", "*" if self.captured else "")

    def __str__(self):
        attrlist = ('flags', 'url', 'imageurl_', 'name')
        return " ".join(filter(bool, [getattr(self, attr) for attr in attrlist]))

    def __init__(self, lat, long, name, **attribs):
        super(Portal, self).__init__(lat, long, **attribs)
        self.name = name

class DB(object):
    portals   = {}
    name      = None
    writeback = False

    filename = '.portaldb'
    _dbname  = 'All Portals'
    datefmt  = '%Y-%m-%d @ %H:%M:%S'

    @staticmethod
    def index(portals):
        if isinstance(portals, dict):
            return portals
        elif isinstance(portals, (list, tuple)):
            return dict([p.indexed() for p in portals])
        elif isinstance(portals, Portal):
            return [portals.indexed()]
        else:
            raise TypeError

    @classmethod
    def load(cls, filename=None, dbname=None, writeback=False):
        filename = filename or cls.filename
        dbname   = dbname   or cls._dbname

        db = open_db(filename)
        try:
            plist = db[dbname]
        except KeyError:
            plist = {}
        db.close()

        this = cls(portals=plist, name=dbname, filename=filename, writeback=writeback)
        eventmsg(this.size, event="loaded into db")
        return this

    @classmethod
    def delete(cls, filename=None, dbname=None):
        filename = filename or cls.filename
        dbname   = dbname   or cls._dbname

        db = open_db(filename, writeback=True)
        try:
            del db[dbname]
        except KeyError:
            pass
        db.close()

    @property
    def list(self):
        return self.portals.values()

    @property
    def size(self):
        return len(self.portals)

    @property
    def boundingBox(self):
        if not self.portals:
            return None
        lats = [float(c[0]) for c in self.portals]
        longs = [float(c[1]) for c in self.portals]
        return (max(lats), min(lats), max(longs), min(longs))

    def save(self, filename=None):
        filename = filename or self.filename

        db = open_db(filename)
        db[self.name] = self.portals
        db.close()

        eventmsg(self.size, event="saved back to db")

    def contains(self, portal):
        return (portal.coords in self.portals)

    def add(self, portals, replace=False, checkneighbours=0):
        portals = self.index(portals)

        if not replace:
            for coords in list(portals.keys()):
                if coords in self.portals:
                    msg_verbose("Duplicate portal found: {}. Skipping.".format(portals[coords]))
                    del portals[coords]

        if checkneighbours:
            existing = self.portals.values()
            for p in portals:
                ns = []
                for e in existing:
                    ns.append((portals[p].distanceTo(e), e.name))
                ns = sorted(ns)[:checkneighbours]
                portals[p].neighbours = ns

                printconsole("{}:{}{}".format(
                    portals[p].name,
                    ("\n" if checkneighbours > 1 else " "),
                    "\n".join(["   {:.2f}m to {}".format(*n) for n in ns])
                ))

        self.portals.update(portals)
        return len(portals)

    def update(self, portals, proplist):
        n = 0
        for portal in portals:
            if portal.coords in self.portals:
                n += 1
                self.portals[portal.coords].update(portal, proplist)
        return n

    def remove(self, portals):
        n = 0
        for portal in portals:
            if portal.coords in self.portals:
                n += 1
                del(self.portals[portal.coords])
        return n

    def removeMatching(self, match, verify=None):
        # The idea being that you supply a predicate, the db finds all the
        #  portals matching the predicate, and sends that off to the function
        #  verify() -- which you also supply. verify() will take a collection
        #  of portals and return a list containing all the ones to be removed
        #  from the db. This allows you to, e.g.: remove a set of portals by
        #  name (which is non-unique) by passing the list of matching portals
        #  off to the user for them to select at run-time which one(s) they
        #  want to remove. Or something.
        #
        # In Haskell-speak, the type signature is:
        #     DB -> (Portal -> Bool) -> ([Portal] -> IO [Portal]) -> IO ()
        #
        # Will also accept the predicate as being a pair of (property, value)
        #  instead of an actual function.
        #

        if isinstance(match, tuple):
            match = lambda p: getattr(p, match[0], None) == match[1]
        verify = verify or (lambda x: x)

        self.remove(verify([p for p in self.list if match(p)]))

    def toCsv(self, timenow=None, foldername=None, s2=None, fields=None):
        if 'latlong' not in fields:
            fields += ',latlong'

        fieldtitles = [
            ('latlong',   "LAT,LONG"),
            ('name',      "NAME"),
            ('visited',   "VISITED"),
            ('captured',  "CAPTURED"),
            ('image',     "IMAGE URL"),
            ('added',     "ADDED"),
            ('neighbour', "NEAREST NEIGHBOUR"),
            ('url',       "INTEL URL")
        ]

        csvtitles = [title for (field, title) in fieldtitles if field in fields]
        csvtitles += ["S2 LVL {} CELL".format(lvl) for lvl in (s2 or [])]

        csvhead = ",".join(csvtitles)
        csvbody = "\n".join([
            p.toCsv(fields=fields, s2=s2) for p in self.portals.values()
        ])
        return "{}\n{}\n".format(csvhead, csvbody)

    def toGeoJSON(self, timenow=None, foldername=None, s2=None, fields=None):
        # TODO
        raise NotImplementedError

    def toJSON(self, timenow=None, foldername=None, s2=None, fields=None):
        if 'latlong' not in fields:
            fields += ',latlong'

        current = max([p.created for p in self.list]).strftime(self.datefmt)
        timenow = (timenow or datetime.today()).strftime(self.datefmt)

        return dump_json({
            'foldername' : foldername or self.name,
            'created'    : '{} MST'.format(timenow),
            'current'    : '{} MST'.format(current),
            'portals'    : [p.asdict(fields, s2=s2) for p in self.portals.values()]
        })

    def toKml(self, timenow=None, foldername=None, s2=None, fields=None):
        timenow = (timenow or datetime.today()).strftime(self.datefmt)
        current = max([p.created for p in self.list]).strftime(self.datefmt)
        foldername = foldername or self.name

        kml = dedent('''
            <?xml version="1.0" encoding="UTF-8"?>
            <!--
                KML generated by /Simple Ingress portal database manager v{0}/
                Contact: Sean Nichols seanni@trichotomy.ca / Ingress: @sirputter

                KML generated {1} MST
                Information understood to be current as of {2} MST
            -->
            <kml xmlns="http://www.opengis.net/kml/2.2"
                 xmlns:gx="http://www.google.com/kml/ext/2.2"
                 xmlns:kml="http://www.opengis.net/kml/2.2"
                 xmlns:atom="http://www.w3.org/2005/Atom">
            <Document>
                <name>{3}</name>
                <Style id="red_stars_normal">
                    <IconStyle>
                        <Icon>
                            <href>http://maps.google.com/mapfiles/kml/paddle/red-stars.png</href>
                        </Icon>
                        <hotSpot x="32" y="1" xunits="pixels" yunits="pixels"/>
                    </IconStyle>
                    <ListStyle>
                        <ItemIcon>
                            <href>http://maps.google.com/mapfiles/kml/paddle/red-stars-lv.png</href>
                        </ItemIcon>
                    </ListStyle>
                </Style>
                <Style id="red_stars_nolabel_normal">
                    <IconStyle>
                        <Icon>
                            <href>http://maps.google.com/mapfiles/kml/paddle/red-stars.png</href>
                        </Icon>
                        <hotSpot x="32" y="1" xunits="pixels" yunits="pixels"/>
                    </IconStyle>
                    <LabelStyle>
                        <color>00ffffff</color>
                    </LabelStyle>
                    <ListStyle>
                        <ItemIcon>
                            <href>http://maps.google.com/mapfiles/kml/paddle/red-stars-lv.png</href>
                        </ItemIcon>
                    </ListStyle>
                </Style>
                <Style id="red_stars_hilight">
                    <IconStyle>
                        <scale>1.2</scale>
                        <Icon>
                            <href>http://maps.google.com/mapfiles/kml/paddle/red-stars.png</href>
                        </Icon>
                        <hotSpot x="32" y="1" xunits="pixels" yunits="pixels"/>
                    </IconStyle>
                </Style>
                <StyleMap id="sm_red_stars">
                    <Pair>
                        <key>normal</key>
                        <styleUrl>#red_stars_normal</styleUrl>
                    </Pair>
                    <Pair>
                        <key>highlight</key>
                        <styleUrl>#red_stars_hilight</styleUrl>
                    </Pair>
                </StyleMap>
                <StyleMap id="sm_red_stars_nolabel">
                    <Pair>
                        <key>normal</key>
                        <styleUrl>#red_stars_nolabel_normal</styleUrl>
                    </Pair>
                    <Pair>
                        <key>highlight</key>
                        <styleUrl>#red_stars_hilight</styleUrl>
                    </Pair>
                </StyleMap>
                <Folder>
                    <name>{3}</name>
                    <visibility>1</visibility>
            ''').format(__version__, timenow, current, foldername).strip()

        placemarks = [
            p.toKml(indent=2, fields=fields, s2=s2)
            for p in self.portals.values()
        ]
        kml += "\n{}".format("\n".join(placemarks))

        kml += dedent('''
                </Folder>
            </Document>
            </kml>
            ''')

        return kml

    def showDetails(self):
        return "\n".join([p.details for p in self.portals])

    def __str__(self):
        return "# {}\n".format(self.name) + "\n".join(map(str, self.portals.values()))

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.writeback:
            self.save(self.filename)

    def __init__(self, portals=None, name=None, filename=None, writeback=False):
        self.name = name or self._dbname
        self.filename = filename or self.filename
        self.writeback = writeback

        portals = portals or {}
        if isinstance(portals, list):
            portals = self.index(portals)

        self.portals = portals


###################################################################################################

def isportal(p):
    if isinstance(p, Portal):
        return p.valid
    return False

def getArgs():
    # Up yours, PEP8

    def mkSubParser(option_text, help_text):
        desc_text = "{}:\n {}".format(parser.description, help_text)
        return subs.add_parser(
            option_text,
            help            = help_text,
            description     = desc_text,
            formatter_class = RawDescriptionHelpFormatter
        )

    PortalFile = FileType('r', encoding='Latin-1')

    parser = ArgumentParser(description = "Simple Ingress portal database manager")
    parser.add_argument('-d','--dbname',  type    = str,
                                          help    = "Name of portal database to read/write")
    parser.add_argument(     '--dbfile',  type    = str,
                                          default = '.portaldb',
                                          help    = "Name of DBF file containing portal database (without extension)")
    parser.add_argument(     '--version', action  = 'version',
                                          version = "%(prog)s {version}".format(version=__version__))
    outputs = parser.add_mutually_exclusive_group()
    outputs.add_argument('-v','--verbose', action  = 'store_true',
                                           help    = "Print extra diagnostic messages")
    outputs.add_argument('-q','--quiet',   action  = 'store_true',
                                           help    = "Print no diagnostic messages")
    subs = parser.add_subparsers(title="commands", dest='action')

    ######## INFO
    parser_info = mkSubParser('info', "Print basic status info about the database")

    ######## ADD
    parser_add = mkSubParser('add', "Add portals to database")
    parser_add.add_argument('portalfile',        type    = PortalFile,
                                                 help    = "File containing list of portals to add ('-' to read from stdin)")
    parser_add.add_argument('-n','--neighbours', type    = int,
                                                 default = 0,
                                                 help    = "Compare to existing portals and store the N closest neighbours")
    parser_add.add_argument('-o','--overwrite',  action  = 'store_true',
                                                 help    = "Completely overwrite any portals that are already in the database")
    parser_add.add_argument('-t','--timestamp',  type    = str,
                                                 default = '',
                                                 help    = "'Created' timestamp to assign to portals - ether unix or YMD_HMS")
    parser_add.add_argument('-u','--unlabeled',  action  = 'store_true',
                                                 help    = "Suppress labels on KML points (e.g. in Google Earth)")

    ######## DELETE
    parser_delete = mkSubParser('delete', "Delete portals from database")
    parser_delete.add_argument('portalfile',    type   = PortalFile,
                                                help   = "File containing list of portals to delete ('-' to read from stdin)")
    parser_delete.add_argument('-n','--byname', action = 'store_true',
                                                help   = "Pass list of portals by name (rather than lat/long coords)")

    ######## DIFF
    parser_diff = mkSubParser('diff', "Compare portals in database to supplied list")
    parser_diff.add_argument('portalfile',         type   = PortalFile,
                                                   help   = "File containing list of portals to compare ('-' to read from stdin)")
    direction = parser_diff.add_mutually_exclusive_group()
    direction.add_argument('-c','--contained',     dest   = 'only_contained',
                                                   action = 'store_true',
                                                   help   = "Only show portals contained in the DB but not in the input file")
    direction.add_argument('-n','--not-contained', dest   = 'only_uncontained',
                                                   action = 'store_true',
                                                   help   = "Only show portals not contained in the DB but in the input file")

    ######## PRINT
    parser_print = mkSubParser('print', "Ouput list of portals in database")
    parser_print.add_argument('-f','--fields',      type    = str,
                                                    default = '',
                                                    help    = "Comma-separated list of fields to include in output")
    parser_print.add_argument('-F','--folder',      type    = str,
                                                    dest    = 'foldername',
                                                    help    = "Name of the KML folder to generate")
    if s2_enabled:
        parser_print.add_argument('-s','--s2cells', type    = str,
                                                    dest    = 's2_levels',
                                                    default = '',
                                                    help    = "Comma-separated list of S2 cell level ids to include")

    geoformat = parser_print.add_mutually_exclusive_group()
    geoformat.add_argument('-c','--csv',            action  = 'store_true',
                                                    help    = "Output database in CSV format")
    geoformat.add_argument('-g','--geojson',        action  = 'store_true',
                                                    help    = "Output database in GeoJSON format")
    geoformat.add_argument('-j','--json',           action  = 'store_true',
                                                    help    = "Output database in plain JSON format")
    geoformat.add_argument('-k','--kml',            action  = 'store_true',
                                                    help    = "Output database in KML format")

    ######## UPDATE
    parser_update = mkSubParser('update', "Update portals in database")
    parser_update.add_argument('-f','--fields',   type    = str,
                                                  default = 'isvalid,name,visited,captured,image',
                                                  help    = "Comma-separated list of fields to update")
    parser_update.add_argument('-p','--personal', action  = 'store_true',
                                                  help    = "Indicates input file contains log (generally 'game_log.tsv') from personal data")
    parser_update.add_argument('portalfile',      type    = PortalFile,
                                                  help    = "File containing list of portals to update ('-' to read from stdin)")

    args = parser.parse_args()
    if args.action not in ('info', 'add', 'delete', 'diff', 'print', 'update'):
        parser.print_help()
        exit(1)

    return args


# Effing Windows. (This is hack to allow us to write UTF8 to console.)
stdout = TextIOWrapper(stdout.buffer, encoding='utf-8')
def printconsole(s='', end='\n', flush=False):
    stdout.write("{}{}".format(s, end))
    if end == '\n' or flush:
        stdout.flush()

def padlist(lst, length, default=None):
    lst.extend([default] * (length - len(lst)))

def pluralize(phrase, n):
    return ("{} {}" if n == 1 else "{} {}s").format(n, phrase)

def eventmsg(n, thing="portal", event="processed", msg_log=msg_verbose):
    msg_log("{} {}".format(pluralize(thing, n), event))

def setlogging(quiet, verbose):
    loglevel = {
        (False, False) : MSG_NORMAL,
        (False, True ) : MSG_VERBOSE,
        (True,  False) : MSG_QUIET,
        (True,  True ) : MSG_QUIET      # what??
    }[(quiet, verbose)]

    configLogging(format='%(message)s', level=loglevel, stream=stderr)

def readPortalfile(portalfile):
    portals = []
    if portalfile:
        with portalfile as f:
            for line in f.readlines():
                p = Portal.read(line)
                if isportal(p):
                    if p in portals:
                        msg_normal("Duplicate portal read: {}. Skipping.\n".format(p))
                    else:
                        portals.append(p)
    return portals

def readPersonalLog(logfile):
    visits   = []
    captures = []
    if logfile:
        with logfile as f:
            f.readline()        # column header
            for line in f.readlines():
                e = PDLogEntry.read(line)
                if e.visited:
                    visits.append(e)
                elif e.captured:
                    captures.append(e)
    return (visits, captures)

def do_info(dbfile, dbname, args):
    whole_db = open_db(dbfile)
    keys = whole_db.keys()
    if keys:
        printconsole("{} found".format(pluralize("portal database", len(keys))))
        for key in keys:
            printconsole("  {!r}: {}".format(key, pluralize("portal", len(whole_db[key]))))
            bbox = DB(portals=whole_db[key]).boundingBox
            if bbox:
                printconsole("    NSEW bounds are: {} {} {} {}".format(*bbox))
    else:
        printconsole("No portal databases found.")
    whole_db.close()

def do_add(dbfile, dbname, args):
    now = datetime.today()

    if args.timestamp:
        success = False
        for parse in (
            lambda ts: datetime.strptime(ts, '%Y%m%d_%H%M%S'),
            lambda ts: datetime.fromtimestamp(int(ts))
        ):
            try:
                now = parse(args.timestamp)
                success = True
                break
            except ValueError:
                continue

            if not success:
                msg_normal("Unrecognized timestamp format: {}".format(args.timestamp))

    Portal.setdefaults({
        'created'   : now,
        'showlabel' : not args.unlabeled
    })

    portals = readPortalfile(args.portalfile)

    with DB.load(filename=dbfile, dbname=dbname, writeback=True) as db:
        n = db.add(sorted(portals), replace=args.overwrite, checkneighbours=args.neighbours)
    eventmsg(n, event="added to db", msg_log=msg_normal)

def do_delete(dbfile, dbname, args):
    portals = readPortalfile(args.portalfile)

    with DB.load(filename=dbfile, dbname=dbname, writeback=True) as db:
        if args.byname:
            # TODO
            raise NotImplementedError
        else:
            n = db.remove(portals)
            sz = db.size
    eventmsg(n, event="removed from db", msg_log=msg_normal)

    if not sz:
        DB.delete(filename=dbfile, dbname=dbname)
        msg_normal("All portals removed; empty database deleted.")

def do_diff(dbfile, dbname, args):
    portals = readPortalfile(args.portalfile)

    with DB.load(filename=dbfile, dbname=dbname, writeback=False) as db:
        if not args.only_uncontained:
            printconsole("# Portals in DB not in input file")
            for portal in db.list:
                if portal not in portals:
                    printconsole(portal)
            printconsole()

        if not args.only_contained:
            printconsole("# Portals in input file not in DB")
            for portal in portals:
                if not db.contains(portal):
                    printconsole(portal)
            printconsole()

def do_print(dbfile, dbname, args):
    now = datetime.today()
    s2_lvl = None
    if s2_enabled:
        try:
            s2_lvl = list(map(int, args.s2_levels.split(',')))
        except ValueError:
            s2_lvl = None

        if s2_lvl:
            global _regioncoverer
            _regioncoverer = RegionCoverer()

    with DB.load(filename=dbfile, dbname=dbname, writeback=False) as db:
        foldername = args.foldername or "Ingress Portals imported {}".format(now.strftime('%Y%m%d'))
        print_args = (now, foldername, s2_lvl, args.fields)
        if args.kml:
            printconsole(db.toKml(*print_args))
        if args.csv:
            printconsole(db.toCsv(*print_args))
        elif args.geojson:
            printconsole(db.toGeoJSON(*print_args))
        elif args.json:
            printconsole(db.toJSON(*print_args))
        else:
            if args.verbose:
                printconsole(db.showDetails())
            else:
                printconsole(db)

def do_update(dbfile, dbname, args):
    if args.personal:
        (visits, captures) = readPersonalLog(args.portalfile)

        visits=[]
        with DB.load(filename=dbfile, dbname=dbname, writeback=True) as db:
            n_visits = db.update([v.toPortal() for v in set(visits)], ['visited'])
            n_captures = db.update([c.toPortal() for c in set(captures)], ['captured'])

        eventmsg(n_visits, event="marked in db as visited", msg_log=msg_normal)
        eventmsg(n_captures, event="marked in db as captured", msg_log=msg_normal)

    elif args.portalfile:
        portals = readPortalfile(args.portalfile)

        with DB.load(filename=dbfile, dbname=dbname, writeback=True) as db:
            n = db.update(portals, args.fields.split(','))
        eventmsg(n, event="updated in db", msg_log=msg_normal)

def main(args):
    setlogging(args.quiet, args.verbose)

    dbfile = args.dbfile
    if dbfile[0] not in ('\\', '/'):
        dbfile = joinpath(dirname(abspath(__file__)), dbfile)
    dbname = args.dbname or 'Calgary Portals'

    do_action = {
        'info'   : do_info,
        'add'    : do_add,
        'delete' : do_delete,
        'diff'   : do_diff,
        'print'  : do_print,
        'update' : do_update
    }[args.action]

    return do_action(dbfile, dbname, args)

###################################################################################################

if __name__  == '__main__':
    main(getArgs())
